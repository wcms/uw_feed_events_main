<?php

/**
 * @file
 * uw_feed_events_main.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_feed_events_main_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  list($module, $api) = func_get_args();
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_feed_events_main_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => "3.0");
  }
}

/**
 * Implements hook_node_info().
 */
function uw_feed_events_main_node_info() {
  $items = array(
    'events_feed' => array(
      'name' => t('Events feed'),
      'base' => 'node_content',
      'description' => t('Feed that pulls in events'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
