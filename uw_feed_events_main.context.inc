<?php

/**
 * @file
 * uw_feed_events_main.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_feed_events_main_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'hmpg_events_context';
  $context->description = 'A feed on the homepage of events';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-homepage_events-block' => array(
          'module' => 'views',
          'delta' => 'homepage_events-block',
          'region' => 'bottomfirst',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('A feed on the homepage of events');
  t('Content');
  $export['hmpg_events_context'] = $context;

  return $export;
}
